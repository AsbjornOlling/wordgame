{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    buildInputs = [
      pkgs.nodejs
      pkgs.python38
      pkgs.python38Packages.gensim
      pkgs.python38Packages.flask
      pkgs.python38Packages.loguru
    ];
    shellHook = 
    ''
      function setup {
        npm install
      }

      function develop {
        `npm bin`/nodemon -e ts,html,py src/ --exec "`npm bin`/tsc --watch --outdir build src/main.ts & python src/game.py"  
      }

      alias tsc=`npm bin`/tsc

      echo "Commands: setup, develop"
    '';
}
