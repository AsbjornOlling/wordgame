This is a prototype for a game where you try to guess a word based on it's distance to words that you provide.

To run it, you first need to download this pre-trained something something:
https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?resourcekey=0-wjGZdNAUop6WykTtMip30g

It's pretty big so I didn't put it in git.

You also need the python package `gensim`
