import os
import random

import gensim
from gensim.models import Word2Vec, KeyedVectors
import gensim.downloader as api
from loguru import logger
from flask import Flask, request, session, render_template

logger.info("loading model")
model = KeyedVectors.load_word2vec_format(
    'GoogleNews-vectors-negative300.bin.gz',
    binary=True,
    limit=100000
)
logger.info("done loading")


app = Flask(__name__)
app.secret_key = os.urandom(256)


@app.route("/guess", methods=("POST",))
def guess():
    """ User submitted guess.
        Return similarity score or 404
    """

    print(f"User guessed: {request.data}")
    userword = request.data.decode("utf-8").strip()

    # set secretword if not set
    if "secretword" not in session:
        session["secretword"] = "dog" # TODO: select a word at random

    secretword = session["secretword"]

    # do win check
    if secretword == userword:
        return "✨ You Win! ✨", 222

    # do similarty check
    try:
        score = model.similarity(secretword, userword)
    except KeyError as e:
        return str(e), 404

    return str(int(10_000 * score)), 200

@app.route("/", methods=("GET", "OPTIONS"))
def index():
    with open("src/index.html", "r") as fh:
        return fh.read(), 200

@app.route("/main.js", methods=("GET",))
def js():
    with open("build/main.js", "r") as fh:
        return fh.read(), 200

if __name__ == '__main__':
    app.run()
