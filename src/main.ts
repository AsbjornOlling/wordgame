interface GameState {
    attempts: number,
    latestResponse?: BackendResponse,
    latestGuess?: string,
}

const initialGameState: GameState = {
    attempts: 0,
}

type BackendResponse = number | "success" | "key-unknown" | "unknown-error";

function submitButton(): HTMLElement {
    return document.getElementById('submit');
}

function feedbackBox(): HTMLElement {
    return document.getElementById('feedback');

}

function guessField(): HTMLInputElement {
    return <HTMLInputElement>document.getElementById("guess");
}

function getGuess(): string {
    return guessField().value
}

function parseBackendResponse(xhr: XMLHttpRequest): BackendResponse {
    const statusCode = xhr.status; 

    if ( statusCode === 200 ) {
        const parsed = parseInt(xhr.responseText)

        if (parsed === NaN) {
            return "unknown-error";
        } else {
            return parsed;
        }
    } else if ( statusCode === 404 ) {
        return "key-unknown";
    } else if ( statusCode === 222 ) {
        return "success";
    } else {
        return "unknown-error";
    }
}


function submitGuess(): void {
    state.attempts++;
    state.latestGuess = getGuess();
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/guess", true);
    xhr.send(getGuess());
    xhr.onload = function () {
        state.latestResponse = parseBackendResponse(xhr);
        if (state.latestResponse != "key-unknown") {
            updateStatus(state);
        }
    }
}

let state: GameState = initialGameState;

function updateStatus(gameState: GameState): void {
    document.getElementById('attempts').innerHTML = gameState.attempts.toString();

    const latestResponse = gameState.latestResponse;

    if (typeof latestResponse === "number") {
        feedbackBox().innerHTML = `'${gameState.latestGuess}' has a score of ${latestResponse}`;
    } else if (latestResponse === undefined) {
       // This is either the initial state or an error 
    } else if (latestResponse === "success") {
        feedbackBox().innerHTML = `Yay! You have guessed the word in ${gameState.attempts} tries.`
        submitButton().hidden = true;
    } else if (latestResponse === "unknown-error") {
        feedbackBox().innerHTML = "An unknown error occured";
    } else if (latestResponse === "key-unknown") {
        feedbackBox().innerHTML = `The word "${gameState.latestGuess}" didn't seem like a real word...`
    } 
}
